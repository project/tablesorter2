(function ($) {
  Drupal.behaviors.tablesorter = {
    attach: function (context, settings) {
      // The sorting is case insensitive and locale aware by default.
      var tsOptions = {
        theme: settings.tablesorter.theme,
        sortReset: true,
        sortLocaleCompare: true,
        ignoreCase: true,
        widgets: ['uitheme'],
      };

      // Some themes need the icon to be added within a header template.
      var needsHeaderTemplate = {
        bootstrap: true,
        dropbox: true,
        grey: true,
        jui: true,
      };
      if (needsHeaderTemplate[tsOptions.theme]) {
        tsOptions.headerTemplate = '{content} {icon}';
      }

      // Adds Tablesorter to tables with "tablesorter" class.
      $(".tablesorter").each(function (i, table) {
        $(table).once('tablesorter').tablesorter(tsOptions);
      });
    }
  };
})(jQuery);
