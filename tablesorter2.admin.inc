<?php

/**
 * Module settings form.
 */
function tablesorter2_settings_form($form, &$form_state) {
  $form['tablesorter2_theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#description' => t('Choose one of the available Tablesorter themes.'),
    '#options' => array(
      'default' => t('Default'),
      'blackice' => t('Blackice'),
      'blue' => t('Blue'),
      'bootstrap' => t('Bootstrap'),
      'dark' => t('Dark'),
      'dropbox' => t('Dropbox'),
      'green' => t('Green'),
      'grey' => t('Grey'),
      'ice' => t('Ice'),
      'jui' => t('jQuery UI'),
      'materialize' => t('Materialize'),
      'metro-dark' => t('Metro Dark'),
    ),
    '#default_value' => variable_get('tablesorter2_theme', 'default'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
